const {Selector} = require('testcafe');

// Selectors

function select(selector) {
    return Selector(selector).with({boundTestRun: testController});
}

exports.phpTravelLogin = {
    url: function() {
        return 'https://www.phptravels.net/login';
    },
    email: function() {
        return select('div.panel-body>div:nth-child(1)>input');
    },
    password: function() {
        return select('div.panel-body>div:nth-child(2)>input');
    },
    loginButton: function() {
        return select('button.btn-action');
    },
    loginErrorMessage: function() {
        return select('iv.alert');
    }
};
