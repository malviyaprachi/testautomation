const {Selector} = require('testcafe');

// Selectors

function select(selector) {
    return Selector(selector).with({boundTestRun: testController});
}

exports.phpTravel = {
    url: function() {
        return 'https://www.phptravels.net';
    },
    hotelLink: function() {
        return select('ul:nth-child(1) > li.main-lnk.active > a');
    },
    hotelcityNameInput: function() {
        return select('#s2id_autogen1 > a');
    },
    checkinInput: function() {
        return select('#dpd1 > div > input');
    },
    checkinInput: function() {
        return select('#dpd2 > div > input');
    },
    travellerInput: function() {
        return select('#travellersInput');
    }
};
