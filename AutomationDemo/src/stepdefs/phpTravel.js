var {Given, When, Then} = require('cucumber');
var Role = require('testcafe').Role;
var phpTravel = require('../pageobjects/*');

Given(/^I am open PHPTravellogin page$/, async function() {
    await testController.navigateTo(phpTravel.phpTravel.url());
});

When(/^I enter Email$/, async function(text) {
    await testController.typeText(phpTravel.phpTravel.email(), text);
});

When(/^I enter Password$/, async function(text) {
    await testController.typeText(phpTravel.phpTravel.password(), text);
});

When(/^I click the login button$/, async function(text) {
    await testController.click(phpTravel.phpTravel.loginButton());
});