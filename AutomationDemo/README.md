# TestCafe and CucumberJS

## Requirements
* TestCafe
* CucumberJS

To install it, use the `npm install` command.

## Running tests

You can run tests by executing the `.\node_modules\.bin\cucumber-js.cmd` or `npm test` commands in command prompt